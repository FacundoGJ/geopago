var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var gulp = require('gulp');
var gutil = require('gulp-util');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', () => {
  return gulp
    .src('src/assets/scss/styles.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(concat('app.min.css'))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('public/css'))
});

var watchLogger = (event) => {
  gutil.log('[' + event.type + '] ' + event.path);
};

gulp.task('watch', ['release'], () => {

  var wSASS = gulp.watch('src/assets/scss/**/*.scss', ['sass']);
  wSASS.on('change add unlink', watchLogger);

});

gulp.task('release', (cb) => {
  runSequence('sass', cb);
});

gulp.task('default', ['release']);