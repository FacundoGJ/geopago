# URL

[http://www.dacu.com.ar/geopago/](http://www.dacu.com.ar/geopago/)

# Commands

## FIRST TIME

```
npm install gulp -g
npm install
```

## Build 

```
yarn run build
```

## DEVELOPMENT

### React
```
npm start
```

### sass watch
```
gulp watch
```