import React, {Component} from 'react';

class Header extends Component
{
    render() {
        return (
            <div id='header' className="container-fluid">
                <img className='request' src={require('../assets/img/visa.png')} alt=""/>
                <p className='request'>Solicitudes</p>
                <div onClick={this.props.newRequest} className="new-request">
                    <p>Crear solicitud</p>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="19.092px" height="19.092px" viewBox="0 0 19.092 19.092"
                    enableBackground="new 0 0 19.092 19.092">
                    <polygon fillRule="evenodd" clipRule="evenodd" fill='#345B99' points="19.092,10.607 19.092,8.485 10.606,8.485 10.606,0 8.485,0
                        8.485,8.485 0,8.485 0,10.607 8.485,10.607 8.485,19.092 10.606,19.092 10.606,10.607 "/>
                    </svg>
                </div>

            </div>
        );
    }
}
export default Header;
