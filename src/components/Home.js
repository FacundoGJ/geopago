import React, {Component} from 'react';
import Popup from './Popup';
import Header from './Header';
import RequestItem from './RequestItem';

class Home extends Component
{
    constructor(props) {
        super(props);
        this.state = {
            requestList: []
        }
        this.onAccept = this.onAccept.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.addRequest = this.addRequest.bind(this);
    }

    componentDidMount() {
        this.setState({
            requestList: [
                {
                    businessName: 'Nexus tecnologías informáticas S.A.',
                    cuit: '33-70996436-0',
                    establishment: '456421654265',
                    date: '15/09/2017',
                    terminal: 0
                }, {
                    businessName: 'Ramiréz González',
                    cuit: '33-56336526-0',
                    establishment: '45655465',
                    date: '16/09/2017',
                    terminal: 0
                }, {
                    businessName: 'Maquinaria Tonax S.A.',
                    cuit: '33-55465526-0',
                    establishment: '45645456 ',
                    date: '15/09/2017',
                    terminal: 0
                }, {
                    businessName: 'Restaurant Bar Aleman',
                    cuit: '33-55465526-0',
                    establishment: '552555465',
                    date: '19/09/2017',
                    terminal: 0
                }, {
                    businessName: 'Empresa Razón Social 2',
                    cuit: '33-56169526-0',
                    establishment: '999555465 ',
                    date: '19/09/2017',
                    terminal: 0
                }
            ]
        });
    }
    onCancel(i) {
        var newrequestList = this.state.requestList;
        newrequestList.splice(i, 1);
        this.setState({
            requestList: newrequestList
        });
    }
    onAccept(i,terminal) {
        var requestItemOut = this.state.requestList[i]
        requestItemOut.terminal = terminal;
        // alert('post>' + JSON.stringify(requestItemOut))

        var newrequestList = this.state.requestList;
        newrequestList.splice(i, 1);
        this.setState({
            requestList: newrequestList
        });
    }
    addRequest(newrequest){
        var newrequestList = this.state.requestList;
        this.state.requestList.splice(newrequestList.length, 0, newrequest);
        this.setState({
            requestList: newrequestList,
            showPopup: false
        });
    }
    renderPopupNewRequest() {
        if (this.state.showPopup) {
            return <Popup
            cansel={()=> this.setState({showPopup: false})}
            add= {this.addRequest}
            />
        }
    }
    
    render() {
        return (
            <div>
                <div>
                    <Header newRequest={()=> this.setState({showPopup: true})}/>
                </div>
                <div className="container">
                    <div className="col-xs-12">
                        {this.state.requestList.map((requestItem, i, arr) => {
                            return (<RequestItem
                                key={requestItem.establishment}
                                index={i}
                                requestItem={requestItem}
                                cancel={this.onCancel}
                                accept={this.onAccept}/>)
                        })}
                    </div>
                </div>
                <div>
                    {this.renderPopupNewRequest()}
                </div>
            </div>
        );
    }
}
export default Home;
