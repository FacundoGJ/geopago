import React, {Component} from 'react';

class RequestItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            terminal: 0,
            showAreYouSure: false
        }
        this.getClassImput = this.getClassImput.bind(this);
        this.onAccept = this.onAccept.bind(this);
        this.onCansel = this.onCansel.bind(this);
        this.terminalChange = this.terminalChange.bind(this);
    }
    getClassImput() {
        if (this.state.terminal !== 0) {
            return 'with-content';
        }
    }
    terminalChange(event) {
        this.setState({terminal: event.target.value});
        
    }
    onAccept() {
        if (this.state.terminal !== 0) {
            this.refs.container.classList += ' fadeOutRight'
            setTimeout(()=>{
                this.props.accept(this.props.index,this.state.terminal);
            }, 1000 );
        } else {
            this.refs.terminal.classList = 'empty'
        }
    }
    onCansel() {
        if(this.state.showAreYouSure){
            this.refs.showAreYouSure.classList += ' fadeOutLeft';
            setTimeout(()=>{
                this.props.cancel(this.props.index);
            }, 1000 );
        } else {
            this.setState({showAreYouSure: true});
        }
    }
    render() {
        if(this.state.showAreYouSure){
            return (
                <div ref={'showAreYouSure'} className='row request-item show-are-you-sure'>
                    <div className='cancel col-xs-1'>
                        <svg className="img-responsive-SVG"  version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="33px" height="33px" viewBox="0 0 33 33"
                            enableBackground="new 0 0 33 33" preserveAspectRatio="xMinYMin">
                            <circle fillRule="evenodd" clipRule="evenodd" fill="#B41638" cx="16.5" cy="16.5" r="16.5"/>
                            <polygon fillRule="evenodd" clipRule="evenodd" fill="#fff" points="24,10.5 22.5,9 16.5,15 10.5,9 9,10.5 15,16.5 9,22.5 
                                10.5,24 16.5,18 22.5,24 24,22.5 18,16.5 "/>
                        </svg>
                    </div>
                    <div className='col-xs-11'>
                        <div className='col-xs-12 border-red'>
                            <div className='col-xs-7'>
                                <p>¿Estás seguro de rechazar la solicitud de este comercio?</p>
                            </div>
                            <div className='col-xs-5'>
                                <p className="refuse" onClick={this.onCansel}>Rechazar</p>
                                <p className="cancel" onClick={()=> this.setState({showAreYouSure: false})}>Cancelar</p>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
        return (
                <div ref={'container'} className='row request-item'>
                    <div className='cancel col-xs-1'>
                        <svg className="img-responsive-SVG cancel-svg" onClick= {this.onCansel} version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="33px" height="33px" viewBox="0 0 33 33"
                        enableBackground="new 0 0 33 33" preserveAspectRatio="xMinYMin">
                            <circle fillRule="evenodd" clipRule="evenodd" fill="#F2F2F2" cx="16.5" cy="16.5" r="16.5"/>
                            <polygon fillRule="evenodd" clipRule="evenodd" fill="#B41638" points="24,10.5 22.5,9 16.5,15 10.5,9 9,10.5 15,16.5 9,22.5 
                                10.5,24 16.5,18 22.5,24 24,22.5 18,16.5 "/>
                        </svg>
                    </div>

                    <div className='col-xs-4'>
                        <h3>{this.props.requestItem.businessName}</h3>
                        <p className='cuit'><span>CUIT: </span>{this.props.requestItem.cuit}</p>
                    </div>
                    <div className='col-xs-3'>
                        <p className='establishment-text'>N° de establecimiento:</p>
                        <p className='establishment'>{this.props.requestItem.establishment}</p>
                        <p className='date'>{this.props.requestItem.date}</p>
                    </div>
                    <div className='col-xs-3'>
                    <input ref={'terminal'} onChange={this.terminalChange} className={this.getClassImput()} placeholder='N° de Terminal' type="number"/>
                    </div>

                    <div className={'accept col-xs-1'} onClick= {this.onAccept}>
                        <svg className={'img-responsive-SVG accept-svg '+this.getClassImput()} version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="33px" height="33px" viewBox="0 0 33 33"
                        enableBackground="new 0 0 33 33" preserveAspectRatio="xMinYMin">
                        <circle fillRule="evenodd" clipRule="evenodd" fill="#F2F2F2" cx="16.5" cy="16.5" r="16.5"/>
                        <polygon fillRule="evenodd" clipRule="evenodd" fill="#71C23B" points="12.393,21.102 7.598,16.307 6,17.905 12.393,24.298 26.093,10.598 24.495,9 "/>
                        </svg>

                    </div>
                </div>
            );
        }
    }
}

export default RequestItem;