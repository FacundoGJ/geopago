import React, {Component} from 'react';

class Popup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newrequest: {
                businessName: 'neverUsed',
                cuit: 'neverUsed',
                establishment: 'neverUsed',
                date: '',
                terminal: 0
            }
        }
        this.setDate = this.setDate.bind(this);
        this.businessNameChange = this.businessNameChange.bind(this);
        this.cuitChange = this.cuitChange.bind(this);
        this.establishmentChange = this.establishmentChange.bind(this);
        this.addNewrequest = this.addNewrequest.bind(this);
    }
    componentDidMount() {
        this.setState({
            newrequest: {
                businessName: 'neverUsed',
                cuit: 'neverUsed',
                establishment: 'neverUsed',
            }
        })
        this.setDate()
    }
    setDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = dd + '/' + mm + '/' + yyyy;

        var newrequest = this.state.newrequest;
        newrequest.date = today;
        this.setState({newrequest});
    }
    businessNameChange(event){
        var newrequest = this.state.newrequest;
        newrequest.businessName = event.target.value;
        this.setState({newrequest});
    }
    cuitChange(event){
        var newrequest = this.state.newrequest;
        newrequest.cuit = event.target.value;
        this.setState({newrequest});
    }
    establishmentChange(event){
        var newrequest = this.state.newrequest;
        newrequest.establishment = event.target.value;
        this.setState({newrequest});
    }
    isEmptyOrNeverUsed(content){
        return (content === 0 || content === '' || content === undefined || content === 'neverUsed');
    }
    isEmpty(content){
        return (content === 0 || content === '' || content === undefined);
    }
    isNeverUsed(content){
        return (content === 'neverUsed');
    }
    getClassImput(content) {
        if (!this.isEmptyOrNeverUsed(content)) {
            return 'with-content';
        } else if(this.isEmpty(content)){
            return 'empty';
        } else if(this.isNeverUsed(content)) {
            return 'never-used';
        }
    }
    addNewrequest(){
        var businessName = this.state.newrequest.businessName;
        var cuit = this.state.newrequest.cuit;
        var establishment = this.state.newrequest.establishment;
        var newrequest = this.state.newrequest;
        if (this.isNeverUsed(businessName)) {
            newrequest.businessName = '';
            this.setState({newrequest});
        };
        if(this.isNeverUsed(cuit)){
            newrequest.cuit = '';
            this.setState({newrequest});
        };
        if(this.isNeverUsed(establishment)){
            newrequest.establishment = '';
            this.setState({newrequest});
        }
        if (!this.isEmptyOrNeverUsed(businessName) && !this.isEmptyOrNeverUsed(cuit) && !this.isEmptyOrNeverUsed(establishment)){
            this.props.add(this.state.newrequest);
        }
    }
    render() {
        return (
            <div id='popup'
            //onClick={this.props.cansel}
            >
                <div className='container-popup'>
                    <div className='container'>
                        <div className='row'>
                            <h3>Crear Solicitud</h3>
                        </div>
                        <div className='row row-padding-horizontal'>
                            <div className='col-xs-12 col-padding-horizontal'>
                                <p>Razón Social</p>
                                <input onChange={this.businessNameChange} placeholder='ej: Nexus S.A.' className={this.getClassImput(this.state.newrequest.businessName)} ref={'businessName'} type="text"/>
                            </div>
                        </div>
                        <div className='row row-padding-horizontal'>
                            <div className='col-sm-6 col-padding-horizontal'>
                                <p>Número de CUIT</p>
                                <input onChange={this.cuitChange} placeholder='00-000000-0' className={this.getClassImput(this.state.newrequest.cuit)} ref={'cuit'} type="text"/>
                            </div>
                            <div className='col-sm-6 col-padding-horizontal'>
                                <p>Número de establecimiento</p>
                                <input onChange={this.establishmentChange} placeholder='0000000-0' className={this.getClassImput(this.state.newrequest.establishment)} ref={'establishment'} type="text"/>
                            </div>
                        </div>
                        <div className='row row-padding-horizontal'>
                            <div className='col-xs-12 col-padding-horizontal'>
                                <div className='border-top'>
                                    <p className='cansel-button' onClick={this.props.cansel}>Cancelar</p>
                                    <p className='addNewrequest-button' onClick= {this.addNewrequest}>Crear</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Popup;